function filterBy(array, type) {
    let newArr = [];
    array.forEach(element => {
        if (element === null) {
            if (type != "null") {
                newArr.push(element);
            }
        }
        else {
            if (typeof (element) != type) {
                newArr.push(element);
            }
        }
    });
    return newArr;
}
let array = [null, undefined, true, false, 0, 12, 55, 'str', 'name', { a: 22, b: 2 }, [1, 2, 6]];
array = filterBy(array, "null");
console.log(array); 

// let arr = ['hello', 'world', 23, '23', null, {num: 1, num3: 3}];

//  function filterBy (array, theType) {
//      let newArr = [];
//      for (let i = 0; i < array.length; i++ )  {
//          if (typeof array[i] !== theType || Boolean(array[i]) === false) {
//              newArr.push(array[i])
//          }
//      }
//  return newArr;
//  }
 
 
//  console.log( filterBy(arr, 'object')); 